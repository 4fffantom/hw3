const mongoose = require('mongoose');

const Truck = mongoose.model('', {
  userId: String,
  completed: {
    type: Boolean,
    default: false,
  },
  text: String,
  createdDate: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
