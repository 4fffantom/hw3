const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
  createdBy: String,
  assigned_to: String,
  type: String,
  status: {
    type: String,
    default: 'IN SERVICE',
  },
  created_date: {
    type: Date,
    default: Date.now(),
  },
});

module.exports = {Truck};
