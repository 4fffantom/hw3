const {Truck} = require('../models/truckModel');

const getUsersTrucks = async (userId) => {
  const trucks = await Truck.find({createdBy: userId});
  if (!trucks) {
    throw new Error('No truck with such id found');
  }
  return {
    trucks,
  };
};

const addTruckForDriver = async (createdBy, data) => {
  const truck = new Truck({...data, createdBy});
  await truck.save();
};

const getUsersTruckById = async (truckId, createdBy) => {
  const truck = await Truck.findOne({_id: truckId, createdBy: createdBy});
  if (!truck) {
    throw new Error('No truck with such id found!');
  }
  return truck;
};

const deleteUsersTruckById = async (truckId, createdBy) => {
  const truck = await Truck.findOne({_id: truckId, createdBy});
  if (!truck) {
    throw new Error('No truck with such id found!');
  }
  await Truck.findOneAndRemove({_id: truckId, createdBy});
};

module.exports = {
  getUsersTrucks,
  addTruckForDriver,
  getUsersTruckById,
  deleteUsersTruckById,
};
