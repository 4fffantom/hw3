const express = require('express');
const router = new express.Router();

const {
  infoUser,
  deleteUser,
  changeUsersPassword,
} = require('../services/usersService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  const user = await infoUser(userId);

  res.status(200).json({user});
}));

router.delete('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;

  await deleteUser(userId);

  res.status(200).json({message: 'Profile deleted successfully'});
}));

router.patch('/password', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const {oldPassword, newPassword} = req.body;

  await changeUsersPassword(userId, oldPassword, newPassword);

  res.status(200).json({message: 'Password changed successfully'});
}));

module.exports = {
  usersRouter: router,
};
