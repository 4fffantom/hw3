const express = require('express');
const router = new express.Router();

const {
  getUsersTrucks,
  addTruckForDriver,
  getUsersTruckById,
  deleteUsersTruckById,
} = require('../services/trucksService');

const {
  asyncWrapper,
} = require('../utils/apiUtils');

router.get('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const trucks = await getUsersTrucks(userId);
  res.status(200).json(trucks);
}));

router.post('/', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const data = req.body;
  await addTruckForDriver(userId, data);
  res.status(200).json({message: 'Success'});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const id = req.params.id;
  const truck = await getUsersTruckById(id, userId);

  res.status(200).json({truck});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const {userId} = req.user;
  const id = req.params.id;
  await deleteUsersTruckById(id, userId);
  res.status(200).json({message: 'Success'});
}));

module.exports = {
  trucksRouter: router,
};
