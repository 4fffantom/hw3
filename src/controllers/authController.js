const express = require('express');
const router = new express.Router();

const {register, login} = require('../services/authService');

const {asyncWrapper} = require('../utils/apiUtils');

router.post('/register', asyncWrapper(async (req, res) => {
  const {email, password, role} = req.body;
  await register({email, password, role});
  res.status(200).json({message: 'Profile created successfully'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
  const {email, password} = req.body;
  const token = await login({email, password});
  res.status(200).json({
    jwt_token: token,
  });
}));

module.exports = {
  authRouter: router,
};
